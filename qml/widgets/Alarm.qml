import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Rectangle {
    width: parent.width
    height: rectAlarm.height+listAlarmView.height+units.gu(3.5)
    color: "transparent"
        Rectangle {
            id: alarm
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width-units.gu(2)
            height: units.gu(12)
            color: "transparent"

    Rectangle{
        id: rectAlarm
        height: units.gu(2.5)
        color: "transparent"
        Icon {
           id: iconAlarm
            width: units.gu(2)
            height: units.gu(2)
            name: "alarm"
            color: launchermodular.settings.textColor
        }
        Label {
            id: titleAlarm
            anchors.left: iconAlarm.right
            anchors.leftMargin: units.gu(1)
            text: i18n.tr("Alarms")
            color: launchermodular.settings.textColor
        }
    }

    Label {
        id: emptyLabel
        fontSize: "small"
        anchors.top: rectAlarm.bottom
        visible: listAlarmView.height == 0
        text: i18n.tr("No active alarm")
        color: launchermodular.settings.textColor
    }


ListView {
  id: listAlarmView
  anchors.top: rectAlarm.bottom
    model: AlarmModel {}
    width: parent.width
    height: contentHeight
    delegate: Item{
      visible: model.enabled
      height: if(model.enabled == false){0}else{textAlarm.contentHeight}
      Text {id: textAlarm; text: date.toLocaleDateString(Qt.locale())+" "+i18n.tr("at")+" "+Qt.formatTime(date); color: launchermodular.settings.textColor; font.pointSize: units.gu(1.2);}
    }
}

    MouseArea {
        anchors.fill: parent
            onClicked:Qt.openUrlExternally("application:///com.ubuntu.clock_clock.desktop")
    }

        }
}
