import QtQuick 2.4
import Ubuntu.Components 1.3
import QtOrganizer 5.0


Item {
    id: calendar


    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: textCalendar.height+listCalendar.height+units.gu(2)
        flickableDirection: Flickable.VerticalFlick
        clip: true
        maximumFlickVelocity : units.gu(10)*100//TODO make it a settings


        Column {
            id: textCalendarColumn
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                rightMargin: units.gu(2)
                leftMargin: units.gu(2)
                topMargin: units.gu(2)
            }


            Rectangle{
                id: textCalendar
                height: units.gu(4)
                Icon {
                   id: iconCalendar
                    width: units.gu(2)
                    height: units.gu(2)
                    name: "event"
                    color: launchermodular.settings.textColor
                }
                Label {
                    id: titleCalendar
                    anchors.left: iconCalendar.right
                    anchors.leftMargin: units.gu(1)
                    text: i18n.tr("Agenda")
                    color: launchermodular.settings.textColor
                }
            }



    OrganizerModel {
        id: organizerModel
            
        startPeriod: {
            return launchermodular.datenow
        }
        
        endPeriod: {
            var date = launchermodular.datenow;
            date.setDate(date.getDate() + launchermodular.settings.limiteDaysCalendar);
            return date
        }

        sortOrders: [
            SortOrder{
              id: sortOrder
                blankPolicy: SortOrder.BlanksFirst
                detail: Detail.EventTime
                field: EventTime.FieldStartDateTime
                direction: Qt.AscendingOrder
            }
        ]

        onExportCompleted: {
            console.log("onExportCompleted")
        }

        onImportCompleted: {
            console.log("onImportCompleted")
        }

        onItemsFetched: {
            console.log("onItemsFetched")
        }

        onModelChanged: {
            console.log("onModelChanged")
            mymodel.clear();
            var count = organizerModel.itemCount
            for ( var i = 0; i < count; i ++ ) {
                var item = organizerModel.items[i];
                if(item.itemType != 505){
                  mymodel.append( {"item": item })
                }
            }
        }
        onDataChanged: {
            console.log("onDataChanged")
        }
        manager: "eds"
    }

        ListModel {
            id: mymodel
        }

        ListView {
          id: listCalendar
            anchors.top: textCalendar.bottom
            width: parent.width
            height: contentHeight
            model: mymodel
            delegate: ListItem {
    height: layout.height + (divider.visible ? divider.height : 0)
    divider.visible: false
    ListItemLayout {
        id: layout
        title.text: item.displayLabel
        title.color: "#FFFFFF"
        subtitle.text: {
                var evt_time = item.detail(Detail.EventTime)
                var starttime = evt_time.startDateTime;
                var endtime = evt_time.endDateTime;

                return starttime.toLocaleTimeString(Qt.locale(), Locale.ShortFormat)+" - "+endtime.toLocaleTimeString(Qt.locale(), Locale.ShortFormat)
              }
        subtitle.color: "#AEA79F"
        summary.text: item.description
        summary.color: "#AEA79F"
        Column {
          SlotsLayout.position: SlotsLayout.Leading
          Text{
            text: {
                    var evt_time = item.detail(Detail.EventTime)
                    var starttime = evt_time.startDateTime;
                    return Qt.formatDateTime(starttime, "d" )
                  }
            font.pointSize: units.gu(2.2)
            font.bold: true
            color: "#ffffff"
          }
          Text{
            text: {
                    var evt_time = item.detail(Detail.EventTime)
                    var starttime = evt_time.startDateTime;
                    return Qt.formatDateTime(starttime, "MMM" )
                  }
            color: "#ffffff"
          }
      }
        Item {
            id: slot
            width: secondLabel.width
            //height: parent.height
            SlotsLayout.overrideVerticalPositioning: true
            Label {
                id: secondLabel
                text: item.location
                color: "#AEA79F"
                fontSize: "small"
                y: layout.mainSlot.y + layout.summary.y
                   + layout.summary.baselineOffset - baselineOffset
            }
        }
    }
}

      }
  }
}



}
