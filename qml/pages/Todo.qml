import QtQuick 2.6
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import "todo"
import Qt.labs.settings 1.0

Item {
    id: todo //------------ Change by name of page
    anchors.fill: parent
    anchors {
        rightMargin: units.gu(2)
        leftMargin: units.gu(2)
        topMargin: units.gu(2)
    }




    ListView{
        id:todoList
        anchors.fill: parent
        anchors {
            rightMargin: units.gu(2)
            leftMargin: units.gu(2)
            topMargin: units.gu(2)
        }

        //headerPositioning: ListView.OverlayHeader

        header:Rectangle {
                id: todo
                height: units.gu(5)
                color: "transparent"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                Rectangle {
                    id: colortodo
                    color: launchermodular.settings.backgroundColor
                    radius: units.gu(1)
                    opacity: 0.3
                    anchors.fill: parent
                }
                TextField {
                    id: todoField
                    anchors {
                        left: parent.left
                        right: iconAddTodo.left
                    }
                    height: todo.height
                    color: launchermodular.settings.textColor
                        background: Rectangle {
                                                height: parent.height
                                                color: "transparent"
                                              }
                    maximumLength: 25
                    placeholderText: i18n.tr("new todo")
                    Keys.onReturnPressed: {
                        TodoModel.save(todoField.text)
                        todoField.text = ""
                    }
                }

                Icon {
                    id: iconAddTodo
                    visible: todoField.text.length >= 2
                    anchors {
                        right: todo.right
                        rightMargin: units.gu(1)
                        leftMargin: units.gu(1)
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height*0.5
                    width: height
                    name: "add"
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            TodoModel.save(todoField.text)
                            todoField.text = ""
                        }
                    }
                }
            }

        model:TodoModel.itemModel
        delegate:CheckDelegate{
                    id: delegate
                    width: parent.width

                    background: Rectangle{
                        anchors.fill: parent
                        color: "transparent"
                    }

                    contentItem: Text {
                         text: name
                         verticalAlignment: Text.AlignVCenter
                         font.strikeout: delegate.checked
                         color: launchermodular.settings.textColor
                    }

                    indicator: CheckBox {
                        checked: delegate.checked
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    checked: done

                    onCheckedChanged: {
                        TodoModel.done(index, checked)
                    }


                    PropertyAnimation on opacity {
                        from:0; to:1
                        duration: 500
                    }

                }

    }



}
