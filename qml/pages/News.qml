import QtQuick 2.4
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import "../widgets"
import QtQuick.Controls 2.2
import QtQuick.XmlListModel 2.0
import AppHandler 1.0
import Terminalaccess 1.0
import Ubuntu.Components 1.3
    
Item {
    id: news
    
	ListView {
		id:mylistview
        anchors.fill: parent
        anchors {
            rightMargin: units.gu(2)
            leftMargin: units.gu(2)
            topMargin: units.gu(2)
        }  
        model:xmlNewsList
		delegate:listItem
		focus:true
	}
    Component {
	id: listItem
	Item {
		id:wrapper
		height: if( wrapper.ListView.isCurrentItem ){ units.gu(18)+description.contentHeight*description.visible }else{ units.gu(13)+description.contentHeight*description.visible }
		width: parent.width
            
		MouseArea {
					anchors.fill: parent
					onClicked: {
						mylistview.currentIndex = index
					}	
				}
        
 Rectangle { 
    id: backgroundNews
    color: "#000000";
    anchors.fill: parent
    anchors.bottomMargin: units.gu(1)
    radius: units.gu(2)  
    opacity: launchermodular.settings.backgroundOpacity
                
		Column {
            id: columnHeader
            anchors.fill: parent
            anchors.leftMargin: units.gu(1)
            anchors.topMargin: units.gu(1)
            anchors.bottomMargin: units.gu(1)
            
			Row {
                    Item {
                        id: imageNews
                        width: if( wrapper.ListView.isCurrentItem ){ columnHeader.width }else{ units.gu(11) }
                        height: if( wrapper.ListView.isCurrentItem ){ units.gu(15) }else{ units.gu(11) }
    
                        
                        Image {
                            id: imgIcons
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width-units.gu(1)
                            height: parent.height-units.gu(1)
                            source: (media)? media : ((iconUri)? iconUri : "assets/fallbackicon.png")
                            visible: false
                            fillMode: Image.PreserveAspectCrop
                        }
                        
                        UbuntuShape {
                            source: imgIcons
                            width: parent.width-units.gu(1)
                            height: parent.height-units.gu(1)
                            radius : "medium"
                            sourceFillMode: UbuntuShape.PreserveAspectCrop
                        }
                        
                    }
				Rectangle {
                    id: titleDateAuthors
                    anchors.left: if( wrapper.ListView.isCurrentItem ){ parent.left }else{ imageNews.right }
                    anchors.bottom: if( wrapper.ListView.isCurrentItem ){ imageNews.top }

					Text {
                        id: dateauthors
						text: "Le 01-03-2019 "+i18n.tr("by")+" "+author
						wrapMode:Text.WordWrap
                        color: launchermodular.settings.textColor
                    }
					Text {
						text: title
                        anchors.top: dateauthors.bottom
                        width: if( wrapper.ListView.isCurrentItem ){ wrapper.width-units.gu(2) }else{ wrapper.width-imageNews.width-units.gu(2) }
						wrapMode:Text.WordWrap
                        color: launchermodular.settings.textColor
                        font.pointSize: units.gu(1.5)    
                        font.bold: true
					}
				}
				
			}
			Text { 
				id:description
                text: shortDesc
				width:wrapper.width-units.gu(2)
                height: contentHeight
				wrapMode:Text.WordWrap
				visible:wrapper.ListView.isCurrentItem
                color: launchermodular.settings.textColor
				MouseArea {
					anchors.fill: parent
					onClicked: {
						console.log("clicked")
						pageStack.push(Qt.resolvedUrl("news/Full.qml"),{fullText: longDesc,title:title})

					}
				}
			}
            
		}
	}	

	}
    }
XmlListModel {

id: xmlNewsList
    //source: "http://www.mysite.com/feed.xml"
    xml: ""
    query: "/entries/entry"

    XmlRole { name: "title"; query: "title/string()" }
    XmlRole { name: "shortDesc"; query: "shortDesc/string()" }
    XmlRole { name: "longDesc"; query: "longDesc/string()" }
    XmlRole { name: "url"; query: "url/string()" }
    XmlRole { name: "iconUri"; query: "iconUri/string()" }
    XmlRole { name: "timestamp"; query: "timestamp/number())" }
    XmlRole { name: "category"; query: "category/string()" }
    XmlRole { name: "author"; query: "author/string()" }
    XmlRole { name: "media"; query: "media/string()" }
	
	Component.onCompleted: {
		console.log("startint");
		for(var i = 0 ; i < AppHandler.appsinfo.length ; i++) {
			if(AppHandler.appsinfo[i].getProp("X-Ubuntu-Launcher") != "")
			{
				Terminalaccess.run(AppHandler.appsinfo[i].getProp("Path")+"/"+AppHandler.appsinfo[i].getProp("X-Ubuntu-Launcher") +" 2> /dev/null")
				xmlNewsList.xml = Terminalaccess.outputUntilEnd();
			}
		}
	}

}
ListModel {
	id: newsList2

	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
}
    ListModel {
	id: newsList

	ListElement {
		title: "Google Duo : les appels audios désormais dispo sur la Google Home"
		shortDesc: "Comme prévu, l'enceinte connectée Google Home permet désormais de passer des appels audios via Google Duo.

Depuis quelques semaines maintenant, Google booste son logiciel de messagerie Google Duo, avec notamment une intégration native du côté de chez OnePlus, et même une version Web disponible depuis quelques jours."


		longDesc: "Google Duo aussi sur Google Home
Évidemment, le géant de Cupertino a également tenu à intégrer sa technologie Duo au sein de sa gamme Home. Ainsi, malgré l'absence de caméra, les enceintes Google Home peuvent désormais passer des appels audio via Google Duo. Cela concerne pour l'heure quelques utilisateurs uniquement, mais la mise à jour devrait rapidement être déployée à plus grande échelle.

Concrètement, pour appeler un contact, il suffit de prononcer la formule « Duo Audio Call » et prononcer le nom du contact de son choix. Lors de la réception d'un appel, un tap permet de prendre l'appel, tandis qu'une pression prolongée permet de le refuser."
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
	ListElement {
		title: "title ... title"
		shortDesc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula. "
		longDesc: "
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquet ante. Aenean eget quam quis augue consequat pretium sit amet semper augue. Sed vitae nunc ultricies, consectetur lorem sed, vulputate tortor. Donec dapibus ligula porttitor tellus fermentum, eget lacinia magna tincidunt. Fusce vitae vehicula arcu. Maecenas nec aliquet orci, sit amet varius lectus. Ut viverra pretium est quis sagittis. Proin a cursus nisl, et porttitor neque. In quis enim sit amet nunc vehicula commodo eget id lacus. Nulla porttitor tellus et interdum euismod. Maecenas suscipit neque porttitor, scelerisque neque a, lacinia tortor. Sed pellentesque erat et commodo vulputate. Maecenas turpis lacus, mattis ac diam eu, pulvinar dignissim nisl. Quisque ac nibh ligula.

Quisque porttitor semper dui eu varius. Aenean porta pharetra lectus, in mattis lorem accumsan non. Maecenas condimentum dolor accumsan, aliquet magna vel, bibendum erat. Nunc consectetur odio eget enim lacinia, id suscipit ante sollicitudin. Phasellus posuere nunc sem, in vulputate urna vehicula vel. Suspendisse vel tellus ac mi vehicula tristique ac ut lacus. Vestibulum sagittis ornare mi sed rhoncus. Curabitur neque dolor, semper ac porta a, vestibulum vel eros. Nulla vitae fermentum lectus. Suspendisse eu placerat risus, ut facilisis ex.

Vivamus pulvinar leo sed augue tincidunt eleifend. Integer pharetra risus et dui lobortis, nec cursus arcu euismod. Maecenas mattis nunc ut nibh laoreet pharetra. Integer viverra elementum nunc quis sodales. Cras ac accumsan lorem. Duis eros mi, malesuada sit amet condimentum ac, ornare sed erat. Duis ut augue hendrerit, gravida nulla vitae, placerat est. Nullam scelerisque bibendum porta. Vestibulum mattis nibh eget laoreet tincidunt. Maecenas dolor massa, rhoncus a dignissim pellentesque, sodales vel arcu. Aliquam erat volutpat. Fusce dui turpis, mattis nec ante nec, dapibus efficitur libero. Maecenas sagittis ligula turpis, non sagittis mi mollis non. Integer sollicitudin bibendum eleifend.

Donec elementum faucibus faucibus. Sed commodo dui sit amet mi laoreet, ac viverra enim ornare. Aenean sollicitudin ornare orci quis dapibus. Vivamus in sodales ex, vitae pulvinar est. Donec non dolor ipsum. Nunc finibus tortor quis aliquam pretium. Vivamus iaculis mi eu consequat dictum. Nullam in lorem consequat neque volutpat pulvinar. Ut eu molestie metus, vitae gravida sapien. Cras at eros non eros consectetur imperdiet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec mauris orci, tincidunt eu ullamcorper a, accumsan a diam. Vestibulum euismod consectetur lorem. Nam mollis maximus ultrices. Ut id dolor diam.

Proin id augue non ipsum scelerisque sodales. Aenean quam tellus, dapibus in lorem vel, feugiat cursus nunc. Donec auctor pretium sapien. Maecenas arcu eros, fringilla sed est ut, pretium elementum est. Mauris sodales vulputate urna, luctus dictum lorem. Integer posuere imperdiet augue. Morbi dapibus, lectus in efficitur porta, felis purus aliquam ante, non commodo tortor turpis vitae nisl. Vivamus elementum efficitur velit, sed faucibus eros tincidunt at. Curabitur a lacinia massa. Aenean eu auctor felis. Aliquam posuere pellentesque ex, ac condimentum elit luctus fringilla. Cras sed rhoncus nisl. Nulla aliquet eros interdum urna varius sodales. Integer non mi vel enim lobortis tincidunt ut sit amet dolor. Suspendisse mattis auctor turpis. "
		url: "application:///launchermodular.ubuntouchfr_ubuntouchfr_0.0.2.desktop"
		iconUri: ""
		timestamp: 1549559247
		categorie: "inutile"
		author: "moi"
		media: ""
	}
    }        
}
