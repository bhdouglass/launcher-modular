#!/bin/bash

mount -o rw,remount /

if [ -f "/home/phablet/.unity8-dash.bak" ] 
then 
    echo '#!/bin/bash
    launched=$(ubuntu-app-list | grep launchermodular.ubuntouchfr)
    appid=$(ubuntu-app-launch-appids | grep launchermodular.ubuntouchfr)
    if [ -z $launched ] && [ -n appid ]
    then
    ubuntu-app-launch $appid
    fi
    ' >> /usr/bin/unity8-dash
    chmod ugo+x /usr/bin/unity8-dash
else  
    mv /usr/bin/unity8-dash /home/phablet/.unity8-dash.bak
    echo '#!/bin/bash
    launched=$(ubuntu-app-list | grep launchermodular.ubuntouchfr)
    appid=$(ubuntu-app-launch-appids | grep launchermodular.ubuntouchfr)
    if [ -z $launched ] && [ -n appid ]
    then
    ubuntu-app-launch $appid
    fi
    ' >> /usr/bin/unity8-dash
    chmod ugo+x /usr/bin/unity8-dash
fi

mount -o ro,remount /
