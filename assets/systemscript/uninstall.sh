#!/bin/bash

mount -o rw,remount /

if [ -f "/home/phablet/.unity8-dash.bak" ] 
then 

    if [ "$(head -n1 /home/phablet/.unity8-dash.bak)" != "#!/bin/bash" ] 
    then 
        mv -f /home/phablet/.unity8-dash.bak /usr/bin/unity8-dash
        rm /home/phablet/.unity8-dash.bak
    else
        rm /home/phablet/.unity8-dash.bak
        apt install unity8 --reinstall -y
    fi
    
else  
    apt install unity8 --reinstall -y
fi


mount -o ro,remount /
